class Client
    constructor: (@options) ->
        @socket = do io.connect
        @network = new Network
        @isHost = no
        @socket.on 'full', (game) =>
            @send game or 'HI'
        @socket.on 'message', @onMessage 

    create: (@game) =>  
        @isHost = yes
        @socket.emit 'create or join', @game  

    #join to exist game
    join: (@game) => 
        connection = new RTCConnection onIceCandidate:@checkIceCandidate 
        #@connection.connection.onicecandidate = @checkIceCandidate
        @socket.emit 'create or join', @game 
        connection.sendOffer @setLocalDescriptionAndSendMessage, @_trace
        @network.add connection

    end: =>
        if @isHost
            @_trace 'isHost'
        do @network.close

    send: (msg) =>
        @network.send msg or 'hi'
        
    _trace: (message) ->
        console.log message

    setLocalDescriptionAndSendMessage: (sessionDescription) =>
        @network.setLocalDescription sessionDescription
        @_trace "setLocalAndSendMessage sending message #{sessionDescription}"
        @socket.emit 'message', sessionDescription

    checkIceCandidate: (event, success, error) =>
        if event.candidate 
            @network.addIceCandidate \
                event.candidate,
                -> console.log 'added',
                -> console.log 'Not added'
            @socket.emit 'message', 
                type: 'candidate',
                label: event.candidate.sdpMLineIndex,
                id: event.candidate.sdpMid,
                candidate: event.candidate.candidate
        else 
            @_trace 'End of candidates.' 

    #events
    onJoin: =>
        @_trace JSON.stringify arguments

    onAnswer: => 
         @_trace JSON.stringify arguments

    onMessage: (message) =>
        @_trace "Client received message:{#message}"
        if message.type is 'offer' and @isHost
            connection = new RTCConnection onIceCandidate:@checkIceCandidate 
            connection.setRemoteDescription \
                (new RTCSessionDescription message ),
                => connection.sendAnswer @setLocalDescriptionAndSendMessage, @_trace ,                    
                -> console.log 'f'
            @network.add connection            
        else if message.type is 'answer' and @network
            @_trace "Answer: #{message}"
            @network.get().setRemoteDescription new RTCSessionDescription message
        else if message.type is 'candidate' and @network
            candidate = new RTCIceCandidate \ 
                sdpMLineIndex: message.label,
                candidate: message.candidate
            @network.addIceCandidate \
                candidate,
                -> console.log 'added',
                -> console.log 'not added'
        else if message is 'bye' 
            do @end 


