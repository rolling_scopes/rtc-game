class Network
	constructor: () ->
		@connections = []

	add: (connections) =>
		if Array.isArray connections
			@connections.concat connection			
		else
			@connections.push  connections

	get: =>
		if @connections.length is 1
            @connections[0]
        else
            @connections	

    send: (msg) =>
    	for connection in @connections
    		connection.send msg

    sendOffer: (onSuccess, onError) =>
    	for connection in @connections
    		connection.sendOffer onSuccess, onError

    close: =>
    	for connection in @connections
    		connection.close
    	@connections = []

    setLocalDescription: (desciption) =>
    	for connection in @connections
    		connection.setLocalDescription desciption 

    addIceCandidate: (candidate, onSucces, onError) =>
    	for connection in @connections
    		connection.addIceCandidate candidate, onSucces, onError





		