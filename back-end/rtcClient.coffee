class Network
    constructor: () ->
        @connections = []

    add: (connections) =>
        if Array.isArray connections
            @connections.concat connection          
        else
            @connections.push  connections

    get: =>
        if @connections.length is 1
            @connections[0]
        else
            @connections    

    send: (msg) =>
        for connection in @connections
            connection.send msg

    sendOffer: (onSuccess, onError) =>
        for connection in @connections
            connection.sendOffer onSuccess, onError

    close: =>
        for connection in @connections
            connection.close
        @connections = []

    setLocalDescription: (desciption) =>
        for connection in @connections
            connection.setLocalDescription desciption 

    addIceCandidate: (candidate, onSucces, onError) =>
        for connection in @connections
            connection.addIceCandidate candidate, onSucces, onError

class RTCConnection
    constructor: (options) ->
        try
            @connection = new webkitRTCPeerConnection {iceServers:[{url:"stun:stun.l.google.com:19302"}]}, {optional: [{RtpDataChannels: true}]}
            @connection.onicecandidate =  options.onIceCandidate #TODO
            @connection.ondatachannel = (event) =>
                @channel = event.channel
                @channel.onmessage = (event) =>
                    try
                        json = JSON.parse event.data    
                        do @msg_handlers[json.msg]
                    catch error
                        onError error 
            
            @channel = @connection.createDataChannel 'name', reliable:false
            @channel.onopen = @onOpen
            @channel.onclose = @onClose
            @channel.onmessage = (msg) -> console.log msg               
            @msg_handlers = options #TODO think *hard* about structure
            
        catch error 
            onError error #TODO 
    

    send: (message) =>
        @channel.send message    

    sendOffer: (onSuccess, onError) =>
        @connection.createOffer onSuccess, onError or @onError

    sendAnswer: (onSuccess, onError) =>
        @connection.createAnswer onSuccess, onError or @onError

    close: =>
        do @connection.close

    setRemoteDescription: (description, onSuccess, onError) =>
        @connection.setRemoteDescription description,onSuccess, onError or @onError

    setLocalDescription: (description, onSuccess, onError) =>
        @connection.setLocalDescription description, onSuccess, onError or @onError

    addIceCandidate: (candidate) => 
        @connection.addIceCandidate candidate 

    on: (msg_name, action) =>
        @msg_handlers[msg_name] = action

    #events
    onOpen: =>
        console.log "Send channel state is: #{@channel.readyState}"

    onClose: =>
        console.log "Send channel state is: #{@channel.readyState}"

    onIceCandidate: (event) ->
        console.log "handleIceCandidate event: #{event}"
    
    onError: (error) ->
        console.log error #TODO send to service ?

class Client
    constructor: (@options) ->
        @socket = do io.connect
        @network = new Network
        @isHost = no
        @socket.on 'full', (game) =>
            @send game or 'HI'
        @socket.on 'message', @onMessage 

    create: (@game) =>  
        @isHost = yes
        @socket.emit 'create or join', @game  

    #join to exist game
    join: (@game) => 
        connection = new RTCConnection onIceCandidate:@checkIceCandidate 
        #@connection.connection.onicecandidate = @checkIceCandidate
        @socket.emit 'create or join', @game 
        connection.sendOffer @setLocalDescriptionAndSendMessage, @_trace
        @network.add connection

    end: =>
        if @isHost
            @_trace 'isHost'
        do @network.close

    send: (msg) =>
        @network.send msg or 'hi'
        
    _trace: (message) ->
        console.log message

    setLocalDescriptionAndSendMessage: (sessionDescription) =>
        @network.setLocalDescription sessionDescription
        @_trace "setLocalAndSendMessage sending message #{sessionDescription}"
        @socket.emit 'message', sessionDescription

    checkIceCandidate: (event, success, error) =>
        if event.candidate 
            @network.addIceCandidate \
                event.candidate,
                -> console.log 'added',
                -> console.log 'Not added'
            @socket.emit 'message', 
                type: 'candidate',
                label: event.candidate.sdpMLineIndex,
                id: event.candidate.sdpMid,
                candidate: event.candidate.candidate
        else 
            @_trace 'End of candidates.' 

    #events
    onJoin: =>
        @_trace JSON.stringify arguments

    onAnswer: => 
         @_trace JSON.stringify arguments

    onMessage: (message) =>
        @_trace "Client received message:{#message}"
        if message.type is 'offer' and @isHost
            connection = new RTCConnection onIceCandidate:@checkIceCandidate            
            connection.setRemoteDescription \
                (new RTCSessionDescription message ),
                => connection.sendAnswer @setLocalDescriptionAndSendMessage, @_trace,                    
                -> console.log 'f'    
            @network.add connection      
        else if message.type is 'answer' and @network
            @_trace "Answer: #{message}"
            @network.get().setRemoteDescription new RTCSessionDescription message
        else if message.type is 'candidate' and @network
            candidate = new RTCIceCandidate \ 
                sdpMLineIndex: message.label,
                candidate: message.candidate
            @network.addIceCandidate \
                candidate,
                -> console.log 'added',
                -> console.log 'not added'
        else if message is 'bye' 
            do @end 

client = new Client {host:{}, player: {} }

startButton = document.getElementById 'startButton'

joinButton = document.getElementById 'joinButton' 

sendButton = document.getElementById 'sendButton' 

startButton.onclick = (cmp) -> client.create 'hi'

sendButton.onclick = (cmp) -> client.send 'test'

joinButton.onclick = (cmp) -> client.join 'hi'