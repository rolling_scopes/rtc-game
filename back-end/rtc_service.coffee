node_static = require 'node-static'
http = require 'http'
file = new node_static.Server
app = http.createServer (req, res) ->
  file.serve req, res

app.listen 2013

io = require('socket.io').listen app

io.sockets.on 'connection', (socket) ->

  # convenience function to log server messages to the client
  log = (args...) ->
    array = ['>>> Message from server: '].concat args
    socket.emit 'log', array

  socket.on 'message', (message) ->
    log 'Got message:', JSON.stringify(message)
    # for a real app, would be room only (not broadcast)
    socket.broadcast.emit 'message', message

  socket.on 'create or join', (room) ->
    numClients = io.sockets.clients(room).length

    log "Room #{room} has #{numClients} client(s)"
    log "Request to create or join room #{room}"

    if numClients is 0
      socket.join room
      socket.emit 'created', room
    else if numClients is 1
      io.sockets.in(room).emit 'join', room
      socket.join room
      socket.emit 'joined', room
    else # max two clients
      socket.emit 'full', room
    socket.emit "emit(): client #{socket.id} joined room #{room}"
    socket.broadcast.emit "broadcast(): client #{socket.id} joined room #{room}"
