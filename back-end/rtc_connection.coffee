#f
class RTCConnection
    constructor: (options) ->
        try
            @connection = new webkitRTCPeerConnection {iceServers:[{url:"stun:stun.l.google.com:19302"}]}, {optional: [{RtpDataChannels: true}]}
            @connection.onicecandidate =  options.onIceCandidate #TODO
            @connection.ondatachannel = (event) =>
                @channel = event.channel
                @channel.onmessage = (event) =>
                    try
                        json = JSON.parse event.data    
                        do @msg_handlers[json.msg]
                    catch error
                        onError error 
            
            @channel = @connection.createDataChannel 'name', reliable:false
            @channel.onopen = @onOpen
            @channel.onclose = @onClose                
            @msg_handlers = options #TODO think *hard* about structure
            
        catch error 
            onError error #TODO 
    

    send: (message) =>
        @channel.send message    

    sendOffer: (onSuccess, onError) =>
        @connection.createOffer onSuccess, onError or @onError

    sendAnswer: (onSuccess, onError) =>
        @connection.createAnswer onSuccess, onError or @onError

    close: =>
        do @connection.close

    setRemoteDescription: (description, onSuccess, onError) =>
        @connection.setRemoteDescription description,onSuccess, onError or @onError

    setLocalDescription: (description, onSuccess, onError) =>
        @connection.setLocalDescription description, onSuccess, onError or @onError

    addIceCandidate: (candidate) => 
        @connection.addIceCandidate candidate 

    on: (msg_name, action) =>
        @msg_handlers[msg_name] = action

    #events
    onOpen: =>
        console.log "Send channel state is: #{@channel.readyState}"

    onClose: =>
        console.log "Send channel state is: #{@channel.readyState}"

    onIceCandidate: (event) ->
        console.log "handleIceCandidate event: #{event}"
    
    onError: (error) ->
        console.log error #TODO send to service ?
        
