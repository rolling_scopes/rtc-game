window.tanksConfig = {
	tileSize: 24, //width and hight in pixels,
	mapSize: 24, //number in tiles

	bulletSize: {
		width: 3,
		heigth:  6
	},
	sprites:{
		bullet_W:{
			x: 0,
			y: 0.65
		},
		bullet_E:{
			x: 0,
			y: 0
		},
		bullet_N:{
			x: 0,
			y: 0.75
		},
		bullet_S:{
			x: 1,
			y: 0.75
		}
	},

	speed:{
		bulletE:{
			x: 4,
			y: 0
		},
		bulletW:{
			x: -4,
			y: 0
		},
		bulletN:{
			x: 0,
			y: -4
		},
		bulletS:{
			x: 0,
			y: 4
		}
	}
}