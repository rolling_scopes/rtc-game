// A village is a tile on the grid that the PC must visit in order to win the game
Crafty.c('Bonus', {
  init: function() {
    this.requires('Actor, spr_bonus');    
    this.UpdateState = function(data){
    	this._disappeared = true;
      //required for removing from gameState
      Crafty.trigger('MergeState', this);    	
    	this.destroy();
    	Crafty.audio.play('knock');
    }
  },

  _kept: false,
  _disappeared: false,
 
  // Process a visitation with this village
  _commonVisit: function() {
  	this._kept = true;
    Crafty.trigger('MergeState', this);
    console.log('BonusVisited');
  }
});

Crafty.c("AmmoBonus", {
	init: function(){
		this.requires('Bonus');
		this.visit = function(that) {
		  	that._ammo += 10;
		  	this._commonVisit();		    
		}
	}
})