_barrier = {
  init: function() {
    this.requires('Actor');
  }
};

_wall = {
  init: function() {
    this.requires('Solid, spr_wall, Barrier');
    this.UpdateState = function(data){  
    }  
  }
};

_breakWall = {
  init: function() {
    this.requires('Solid, Health, spr_brWall, Barrier, SpriteAnimation').setHealth(3)
      .reel('spr_brWall3', 4, 1, 0, 2)
      .reel('spr_brWall2', 4, 0, 1, 2)
      .reel('spr_brWall1', 4, 1, 1, 2);

    this.UpdateState = function(data){    
      if(data._health > 0){
        this.animate('spr_brWall' + data._health, -1);
      }
      _health.UpdateState.call(this, data);
    }
  }
};

_bush = {
  init: function() {
    this.requires('spr_bush, Barrier');
    this.UpdateState = function(data){
    }
  }  
};

_water = {
  init: function() {
    this.requires('Solid, spr_water, Barrier');
    this.UpdateState = function(data){
    }
  }  
};

_ice = {
  init: function() {
    this.requires('spr_ice, Barrier');
    this.UpdateState = function(data){
    }
  }
};

Crafty.c('Barrier', _barrier);

Crafty.c('Wall', _wall);
 
Crafty.c('BreakWall', _breakWall);
 
Crafty.c('Bush', _bush);

Crafty.c('Water', _water);

Crafty.c('Ice', _ice);