Crafty.c('RemoteControlled', {
	init: function(){
		this.bind('UpdateState',function(data){
			this._updateState(data);
		});

		this.Id = generateId();
	},

  	_updateState: function(data){
  		if(data[this.Id]){
      		this.UpdateState(data[this.Id]);      
    	}
	}

});

