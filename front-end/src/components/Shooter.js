Crafty.c('Shooter', {
  init: function() {    
    this.bind("KeyUp", function (e) {
      if (e.key==32) {
        this.shoot();        
      }
    });    
  },

  _ammo: 10,

  shoot: function(){
    if(this._ammo > 0){
      this._ammo = this._ammo - 1;      
      
      var or = this.orientation;
      var x = (or == 'E') ? tanksConfig.tileSize + tanksConfig.tileSize/2 : (or == 'W') ? - tanksConfig.bulletSize.heigth - tanksConfig.tileSize/2 : tanksConfig.tileSize/2 - 2;
      var y = (or == 'S') ? tanksConfig.tileSize + tanksConfig.tileSize/2 : (or == 'N') ? - tanksConfig.bulletSize.heigth - tanksConfig.tileSize/2 : tanksConfig.tileSize/2 - 2;
      var bullet = Crafty.e('Bullet').setPower(1).setDirection(or).attr({ x: this.x + x, y: this.y + y }).keepMove();
      
      console.log('ammo = ' + this._ammo);
      Crafty.trigger('MergeState', this);
      Crafty.trigger('MergeState', bullet);
    }
  },
});