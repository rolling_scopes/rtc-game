_health = {
  init: function(){  
  },

  UpdateState : function(data){      
    if(data._health <=0){
      this._disappeared = true;
      this.trigger('MergeState', this);     
      this.destroy();
      Crafty.audio.play('knock');  
    }
  },
  
  _health: 5,
  setHealth: function(val){
    this._health = val;
    return this;
  },

  hurtHealth: function(data){
    this._health -= data;
    Crafty.trigger('MergeState', this);    
    console.log('health=' + this._health);    
  }
}

Crafty.c('Health', _health);

