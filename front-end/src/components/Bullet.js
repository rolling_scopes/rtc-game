Crafty.c('Bullet', {
  init: function() {
    this.requires('2D, Canvas, SpriteAnimation, Collision, RemoteControlled')
    .onHit('Wall', this.hitWallBar)
    .onHit('Health', this.hitHealthBar);

    //method for hostBullet & clientBullet
    this.UpdateState = function(data){
      this.x = Math.round((data.position.x + data._speed.x) * 1000) / 1000;
      this.y = Math.round((data.position.y + data._speed.y) * 1000) / 1000;
    }
  },

  _power: 1,
  _moveDescriptor: null,
  _animation_speed:4,
  _direction: 'N',
  _speed: {x: 0, y: -2},

  //method for hostBullet
  setDirection: function(or){
    this._direction = or;
    this._speed = tanksConfig.speed['bullet' + or];
    this.requires('spr_bullet_' + or).reel('bulletMoving' + or, this._animation_speed, tanksConfig.sprites['bullet_' + or].x, tanksConfig.sprites['bullet_' + or].y,   2);
    var self = this;
    return this;
  },

  //method for hostBullet
  setPower: function(val){
    this._power = val;
    return this;
  },

  //method for hostBullet
  hitHealthBar: function(data) { 
    health = data[0].obj;
    health.hurtHealth(this._power);

    this._commonHit();
  },

  //method for hostBullet
  hitWallBar: function(data){
    this._commonHit();    
  },

  //method for hostBullet
  _commonHit: function(){ 
    clearInterval(this._moveDescriptor);
    Crafty.audio.play('knock');

    this._disappeared = true;
    Crafty.trigger('MergeState', this);
    this.destroy();
  },

  //method for hostBullet
  keepMove:function(){
    var self = this;
    self.position = {x: self.x, y: self.y};
    this._moveDescriptor = setInterval(function(){
      self.position.x = Math.round((self.x + self._speed.x) * 1000) / 1000;
      self.position.y = Math.round((self.y + self._speed.y) * 1000) / 1000;
      Crafty.trigger('MergeState', self);     
    }, 10);
    return this;
  }
});