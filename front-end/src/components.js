// This is the player-controlled character
Crafty.c('PlayerCharacter', {
  init: function() {
    var animation_speed = 1000;

    this.requires('Actor, Fourway, Collision, spr_player, SpriteAnimation, Shooter, Health')
      .fourway(2)
      .stopOnSolids()
      .onHit('Bonus', this.getBonus)
      .reel('PlayerMovingUp5', animation_speed, 0, 0, 2)
      .reel('PlayerMovingRight5', animation_speed, 1, 0, 2)
      .reel('PlayerMovingDown5', animation_speed, 2, 0, 2)
      .reel('PlayerMovingLeft5', animation_speed, 3, 0, 2)
      .reel('PlayerMovingUp4', animation_speed, 0, 0, 2)
      .reel('PlayerMovingRight4', animation_speed, 1, 0, 2)
      .reel('PlayerMovingDown4', animation_speed, 2, 0, 2)
      .reel('PlayerMovingLeft4', animation_speed, 3, 0, 2)
      .reel('PlayerMovingUp3', animation_speed, 0, 0, 2)
      .reel('PlayerMovingRight3', animation_speed, 1, 0, 2)
      .reel('PlayerMovingDown3', animation_speed, 2, 0, 2)
      .reel('PlayerMovingLeft3', animation_speed, 3, 0, 2)
      .reel('PlayerMovingUp2', animation_speed, 0, 1, 2)
      .reel('PlayerMovingRight2', animation_speed, 1, 1, 2)
      .reel('PlayerMovingDown2', animation_speed, 2, 1, 2)
      .reel('PlayerMovingLeft2', animation_speed, 3, 1, 2)
      .reel('PlayerMovingUp1', animation_speed, 0, 2, 2)
      .reel('PlayerMovingRight1', animation_speed, 1, 2, 2)
      .reel('PlayerMovingDown1', animation_speed, 2, 2, 2)
      .reel('PlayerMovingLeft1', animation_speed, 3, 2, 2);

    this.UpdateState  = function(data){
      if (data._movement.x > 0) {
        this.animate('PlayerMovingRight' + data._health, -1);
        this.orientation = 'E';
      } else if (data._movement.x < 0) {
        this.animate('PlayerMovingLeft'+ data._health, -1);
        this.orientation = 'W';
      } else if (data._movement.y > 0) {
        this.animate('PlayerMovingDown'+ data._health, -1);
        this.orientation = 'S';
      } else if (data._movement.y < 0) {
        this.animate('PlayerMovingUp'+ data._health, -1);
        this.orientation = 'N';
      } else {
        this.pauseAnimation();
      }

      _health.UpdateState.call(this, data);   
    };
 
    this.bind('NewDirection', function(data) {
      Crafty.trigger('MergeState', this);
    });

  },

  orientation: 'N', 

  //method for hostPlayer
  stopOnSolids: function() {
    this.onHit('Solid', this.stopMovement); 
    Crafty.trigger('MergeState', this);
    return this;
  },
 
  //method for hostPlayer
  stopMovement: function() {
    this._speed = 0;
    if (this._movement) {
      this.x -= this._movement.x;
      this.y -= this._movement.y;
    }
    Crafty.trigger('MergeState', this);
  },

  //method for hostPlayer
  getBonus: function(data) {    
    bonus = data[0].obj;
    bonus.visit(this);
    Crafty.trigger('MergeState', this);
  }
});