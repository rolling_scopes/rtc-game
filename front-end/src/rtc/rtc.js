rtc = {
	gameState: {},
	mergeState: function(e){ 
		var obj = this.gameState[e.Id] || {};
		this.gameState[e.Id] = obj;
		
		obj._disappeared = e._disappeared; 
		if(e.__c.Health){
			obj._health = e._health;
		}

		if(e.__c.Fourway){
			obj._movement = e._movement;
		}

		if(e.__c.Bonus){				
			obj._kept = e._kept;				
		}

		if(e.__c.Shooter){
			obj._ammo = e._ammo;
		}

		if(e.__c.Bullet){
			obj._speed = e._speed;
			obj._power = e._power;
			obj._direction = e._direction;
			obj.position = e.position;
		}
	},

	send: function(e){
		rtc.mergeState(e);
	}
}

setInterval(
	function(){
		var state = rtc.gameState;

		//emulation of network delay
		setTimeout(
			function(){
	    		Crafty.trigger('Message', state);
	    		for(var i in state){
	    			if(state[i]._disappeared){
	    				delete state[i];
	    			}
	    		}
	    	},
	    	Math.random()*20);
	},
50);